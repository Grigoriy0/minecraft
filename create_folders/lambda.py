import os
import json

EFS_FOLDER = "/mnt/minecraft"

folders = [
    f"{EFS_FOLDER}/plugins",
    f"{EFS_FOLDER}/world",
    f"{EFS_FOLDER}/world_nether",
    f"{EFS_FOLDER}/world_the_end",
    f"{EFS_FOLDER}/libraries"
]


def run(event, context):
    result = ""
    print("Start")
    try:
        for dir in folders:
            if not os.path.exists(dir):
                print("Creating directory ", dir)
                result += f"Directory {dir} was created.\n"
                os.makedirs(dir)
    except Exception as e:
        print(e)
        result += f"Something went wrong: {e}\n"
        return {
            'statusCode': 200,
            'body': result
        }
    if result == "":
        result = "Directories are present\n"
    return {
        'statusCode': 200,
        'body': result + "\nOK"
    }

