variable "subnet_id" {
  type = string
}

variable "efs_id" {
  type = string
}

variable "tags" {
  type = map(string)
}

variable "security_group" {
  type = string
}

variable "lambda_efs_ap_arn" {
  type = string
}