
data "http" "invoke_function" {
  url = aws_lambda_function_url.invoke.function_url

  depends_on = [var.efs_id, aws_lambda_function_url.invoke]
}


output "result" {
  value = data.http.invoke_function.body
}