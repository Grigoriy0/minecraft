data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

resource "aws_iam_role" "lambda" {
  name               = "mcv2-create-folders-lambda-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  inline_policy {
    name = "logs-efs"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["efs:*"]
          Effect   = "Allow"
          Resource = "*"
        },
        {
          Action = ["ec2:CreateNetworkInterface", "ec2:DescribeNetworkInterfaces", "ec2:DeleteNetworkInterface"]
          Effect = "Allow"
          Resource = ["*"]
        },
      ]
    })
  }
}

data "archive_file" "lambda" {
  type        = "zip"
  source_file = "${path.module}/lambda.py"
  output_path = "${path.module}/lambda.zip"
}

resource "aws_lambda_function" "efs" {
  function_name = "mcv2-create-folders"
  role          = aws_iam_role.lambda.arn
  filename      = data.archive_file.lambda.output_path
  handler       = "lambda.run"
  runtime       = "python3.8"
  source_code_hash = filebase64sha256("${path.module}/lambda.zip")

  vpc_config {
    security_group_ids = [var.security_group]
    subnet_ids         = [var.subnet_id]
  }

  file_system_config {
    arn              = var.lambda_efs_ap_arn
    local_mount_path = "/mnt/minecraft"
  }
  tags = var.tags
}

resource "aws_lambda_function_url" "invoke" {
  function_name      = aws_lambda_function.efs.function_name
  authorization_type = "NONE"
  depends_on = [aws_lambda_function.efs]
}
