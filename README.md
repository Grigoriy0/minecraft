# Minecraft


### Local build and test

```shell
echo "rcon.password=password" >> image/config/server.properties
docker-compose build
docker-compose up -d
```

### How to deploy new image version:
 1 create new tag with larger version
 2 wait while job named "new_app_version" will be green
 3 in case of failure ask help

### How to recreate world(new season):
 1 Clone the repository
 2 Update variable "world_tag" in `variales.tf` file and ask [Grigoriy0](https://gitlab.com/Grigoriy0) to redeploy terraform
