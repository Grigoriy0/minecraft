module "cloud" {
  source    = "./cloud"
  subnet_id = var.subnet_id
  tags      = merge(var.tags, { project-code = "cloud" })
}

module "storage" {
  source         = "./storage"
  subnet_id      = var.subnet_id
  security_group = aws_security_group.mc.id
  world_tag      = var.world_tag
  tags           = var.tags
}

module "create_folders" {
  count = var.create_folders ? 1 : 0

  source           = "./create_folders"
  subnet_id        = var.subnet_id
  security_group   = aws_security_group.mc.id
  efs_id           = module.storage.efs_id
  lambda_efs_ap_arn = module.storage.efs_main_access_point_arn
  tags             = var.tags

  depends_on = [module.storage]
}

resource "aws_lb_listener" "mc" {
  load_balancer_arn = module.cloud.alb_arn
  port              = var.mc_port
  protocol          = "TCP"
  default_action {
    target_group_arn = aws_lb_target_group.mc.arn
    type             = "forward"
  }
  tags = var.tags
  lifecycle {
    replace_triggered_by = [aws_lb_target_group.mc]
  }
}

resource "aws_lb_listener" "rcon" {
  load_balancer_arn = module.cloud.alb_arn
  port              = var.rcon_port
  protocol          = "TCP"
  default_action {
    target_group_arn = aws_lb_target_group.rcon.arn
    type             = "forward"
  }
  tags = var.tags
  lifecycle {
    replace_triggered_by = [aws_lb_target_group.rcon]
  }
}

data "aws_subnet" "s" {
  id = var.subnet_id
}

resource "aws_lb_target_group" "mc" {
  name        = "mcv2-mc-port-tg"
  vpc_id      = data.aws_subnet.s.vpc_id
  protocol    = "TCP"
  target_type = "ip"
  port        = var.mc_port
  tags        = var.tags
  health_check {
    interval = 30
    protocol = "TCP"
    port     = "traffic-port"
  }
}

resource "aws_lb_target_group" "rcon" {
  name        = "mcv2-rcon-port-tg"
  vpc_id      = data.aws_subnet.s.vpc_id
  protocol    = "TCP"
  target_type = "ip"
  port        = var.mc_port
  tags = var.tags
  health_check {
    interval = 30
    protocol = "TCP"
    port     = "traffic-port"
  }
}


resource "aws_ecs_service" "mc" {
  name            = "mcv2-service"
  task_definition = "${aws_ecs_task_definition.mc.family}:${data.aws_ecs_task_definition.latest.revision}"
  desired_count   = var.start_minecraft ? 1 : 0
  cluster         = module.cloud.ecs_cluster_name
  launch_type     = "FARGATE"

  deployment_maximum_percent         = 150
  deployment_minimum_healthy_percent = 100
  health_check_grace_period_seconds  = 120

  network_configuration {
    assign_public_ip = true
    subnets          = [var.subnet_id]
    security_groups  = [aws_security_group.mc.id]
  }
  load_balancer {
    container_name   = "minecraft"
    container_port   = var.mc_port
    target_group_arn = aws_lb_target_group.mc.arn
  }
  load_balancer {
    container_name   = "minecraft"
    container_port   = var.rcon_port
    target_group_arn = aws_lb_target_group.rcon.arn
  }
  enable_ecs_managed_tags = true
  propagate_tags          = "SERVICE"
  tags                    = var.tags

  depends_on = [module.cloud, module.storage]
  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }
}


resource "aws_security_group" "mc" {
  name        = "mcv2-sg"
  description = "Security group for minecraft v2"
  ingress {
    from_port        = var.mc_port
    protocol         = "TCP"
    to_port          = var.mc_port
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port        = 2049
    protocol         = "TCP"
    to_port          = 2049
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port        = 25575
    protocol         = "TCP"
    to_port          = 25575
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    description      = "RCON port"
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = var.tags
}

resource "aws_cloudwatch_log_group" "mc" {
  name = "mcv2"
  tags = var.tags
}
