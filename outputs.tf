output "alb_dns_name" {
  value = module.cloud.alb_dns_name
}

output "ecr_url" {
  value = module.storage.repository_url
}

output "efs_directories" {
  value = var.create_folders ? module.create_folders[0].result : null
}

output "efs_dns_name" {
  value = module.storage.efs_dns_name
}
