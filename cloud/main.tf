variable "subnet_id" {}

variable "tags" {}

resource "aws_lb" "main" {
  load_balancer_type = "network"
  name               = "dog0-sd"
  subnet_mapping {
    subnet_id = var.subnet_id
  }
  tags = var.tags
}

resource "aws_ecs_cluster" "main" {
  name = "main"
  tags = var.tags
}

resource "aws_ecs_cluster_capacity_providers" "spots" {
  cluster_name = aws_ecs_cluster.main.name
  capacity_providers = ["FARGATE_SPOT"]
}

output "alb_dns_name" {
  value = aws_lb.main.dns_name
}

output "alb_arn" {
  value = aws_lb.main.arn
}

output "ecs_cluster_name" {
  value = aws_ecs_cluster.main.name
}