
output "repository_url" {
  value = aws_ecr_repository.mc.repository_url
}

output "ecr_name" {
  value = aws_ecr_repository.mc.name
}

output "efs_id" {
  value = aws_efs_file_system.efs.id
}

output "efs_dns_name" {
  value = aws_efs_file_system.efs.dns_name
}

output "efs_mt_ip_address" {
  value = aws_efs_mount_target.mt.ip_address
}

output "efs_main_access_point_arn" {
  value = aws_efs_access_point.root_for_lambda.arn
}

output "efs_access_points" {
  value = [
    aws_efs_access_point.world.root_directory[0].path,
    aws_efs_access_point.world_nether.root_directory[0].path,
    aws_efs_access_point.world_the_end.root_directory[0].path,
    aws_efs_access_point.libraries.root_directory[0].path,
    aws_efs_access_point.plugins.root_directory[0].path,
  ]
}
