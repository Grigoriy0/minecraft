
resource "aws_ecr_repository" "mc" {
  name = "mcv2"
  tags = var.tags
}

resource "aws_ecr_lifecycle_policy" "mc" {
  repository = aws_ecr_repository.mc.name

  policy     = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Keep last 4 images",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 4
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
  depends_on = [aws_ecr_repository.mc]
}


resource "aws_efs_file_system" "efs" {
  creation_token = var.world_tag
  tags = var.tags
}

resource "aws_efs_mount_target" "mt" {
  file_system_id = aws_efs_file_system.efs.id
  subnet_id      = var.subnet_id
  security_groups = [var.security_group]
}

resource "aws_efs_access_point" "root_for_lambda" {
  file_system_id = aws_efs_file_system.efs.id
  root_directory {
    path = "/minecraft"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = "0777"
    }
  }

  posix_user {
    gid = 1000
    uid = 1000
  }
  tags = var.tags
}


resource "aws_efs_access_point" "world" {
  file_system_id = aws_efs_file_system.efs.id
  root_directory {
    path = "/minecraft/world"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = "0777"
    }
  }

  posix_user {
    gid = 1000
    uid = 1000
  }
  tags = var.tags
}

resource "aws_efs_access_point" "world_nether" {
  file_system_id = aws_efs_file_system.efs.id
  root_directory {
    path = "/minecraft/world_nether"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = "0777"
    }
  }

  posix_user {
    gid = 1000
    uid = 1000
  }
  tags = var.tags
}

resource "aws_efs_access_point" "world_the_end" {
  file_system_id = aws_efs_file_system.efs.id
  root_directory {
    path = "/minecraft/world_the_end"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = "0777"
    }
  }

  posix_user {
    gid = 1000
    uid = 1000
  }
  tags = var.tags
}

resource "aws_efs_access_point" "libraries" {
  file_system_id = aws_efs_file_system.efs.id
  root_directory {
    path = "/minecraft/libraries"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = "0777"
    }
  }

  posix_user {
    gid = 1000
    uid = 1000
  }
  tags = var.tags
}

resource "aws_efs_access_point" "plugins" {
  file_system_id = aws_efs_file_system.efs.id
  root_directory {
    path = "/minecraft/plugins"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = "0777"
    }
  }

  posix_user {
    gid = 1000
    uid = 1000
  }
  tags = var.tags
}
