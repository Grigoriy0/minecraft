terraform {
  backend "s3" {
    key = "terraform-state/minecraft.tfstate"
    region = "us-east-1"
    encrypt = true
  }
  required_version = "~> 1.2.0"
}

provider "aws" {
  region = "eu-north-1"
}
