variable "mc_port" {
  default = 25565
}

variable "rcon_port" {
  default = 25575
}

variable "world_tag" {
  type    = string
  default = "mcv2"
}

variable "task_memory" {
  default = "8192"
}

variable "task_cpu" {
  default = "1024"
}

variable "subnet_id" {
  type    = string
}

variable "create_folders" {
  default = false
}

variable "start_minecraft" {
  default = true
}

variable "tags" {
  default = {
    project-code = "mcv2"
    client       = "me"
    source       = "Terraform"
    season       = "2"
  }
}
