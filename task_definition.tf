data "aws_ecs_task_definition" "latest" {
  task_definition = aws_ecs_task_definition.mc.family
  depends_on = [aws_ecs_task_definition.mc]
}

resource "aws_ecs_task_definition" "mc" {
  container_definitions    = local.container_definitions
  family                   = "mcv2"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = var.task_memory
  cpu                      = var.task_cpu
  execution_role_arn       = aws_iam_role.execution.arn
  volume {
    name = "world"
    efs_volume_configuration {
      file_system_id = module.storage.efs_id
      root_directory = module.storage.efs_access_points[0]
    }
  }
  volume {
    name = "world_nether"
    efs_volume_configuration {
      file_system_id = module.storage.efs_id
      root_directory = module.storage.efs_access_points[1]
    }
  }
  volume {
    name = "world_the_end"
    efs_volume_configuration {
      file_system_id = module.storage.efs_id
      root_directory = module.storage.efs_access_points[2]
    }
  }
  volume {
    name = "libraries"
    efs_volume_configuration {
      file_system_id = module.storage.efs_id
      root_directory = module.storage.efs_access_points[3]
    }
  }
  volume {
    name = "plugins"
    efs_volume_configuration {
      file_system_id = module.storage.efs_id
      root_directory = module.storage.efs_access_points[4]
    }
  }
  tags = var.tags

  lifecycle {
    ignore_changes = [container_definitions]
  }
}

locals {
  container_definitions = jsonencode([
    {
      name         = "minecraft"
      image        = "${module.storage.repository_url}:${var.world_tag}"
      essential    = true
      portMappings = [
        {
          containerPort = var.mc_port
          hostPort      = var.mc_port
        },
        {
          containerPort = 2049
          hostPort      = 2049
        },
        {
          containerPort = var.rcon_port
          hostPort      = var.rcon_port
        }
      ]
      mountPoints = [
        {
          containerPath = "/opt/minecraft/plugins"
          sourceVolume  = "plugins"
        },
        {
          containerPath = "/opt/minecraft/libraries"
          sourceVolume  = "libraries"
        },
        {
          containerPath = "/opt/minecraft/world"
          sourceVolume  = "world"
        },
        {
          containerPath = "/opt/minecraft/world_nether"
          sourceVolume  = "world_nether"
        },
        {
          containerPath = "/opt/minecraft/world_the_end"
          sourceVolume  = "world_the_end"
        },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options   = {
          "awslogs-group"         = aws_cloudwatch_log_group.mc.name
          "awslogs-region"        = data.aws_region.current.name
          "awslogs-stream-prefix" = "mcv2"
        }
      }
    }
  ])
}
