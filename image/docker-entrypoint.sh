#!/usr/bin/env bash

echo "Check that the kernel is present"

if [[ ! -f "/opt/minecraft/server.jar" ]]; then
  echo "Error: kernel not present"
  exit 1
fi

echo "Check that plugins are present"
if [[ ! -f "/opt/minecraft/plugins/SkinsRestorer.jar" ]]; then
  echo "WARNING: Downloading SkinsRestorer.jar"
  wget -O plugins/SkinsRestorer.jar https://github.com/SkinsRestorer/SkinsRestorerX/releases/download/14.2.1/SkinsRestorer.jar
fi
if [[ ! -f "/opt/minecraft/plugins/Chunky.jar" ]]; then
  echo "WARNING: Downloading Chunky.jar"
  wget -O plugins/Chunky.jar https://ci.codemc.io/view/Author/job/pop4959/job/Chunky/340/artifact/bukkit/build/libs/Chunky-1.3.13.jar
fi
if [[ ! -f "/opt/minecraft/plugins/TabTPS.jar" ]]; then
  echo "WARNING: Downloading TabTPS.jar"
  wget -O plugins/TabTPS.jar https://github.com/jpenilla/TabTPS/releases/download/v1.3.14/tabtps-spigot-1.3.14.jar
fi

echo "========================================="
echo "cat bukkit.yml:"
cat bukkit.yml || true
echo "========================================="
echo "cat paper.yml:"
cat paper.yml || true
echo "========================================="
echo "cat spigot.yml:"
cat spigot.yml || true

echo "========================================="
echo "========================================="
echo "========================================="
echo "Starting the kernel"
exec java "$@"
