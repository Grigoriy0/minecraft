#!/usr/bin/fish

tf taint module.storage.aws_efs_file_system.efs
TF_VAR_create_folders=true tf apply -auto-approve
TF_VAR_create_folders=false tf apply -auto-approve